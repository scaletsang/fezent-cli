%title: How to add configuration files?
%author: Anthony Tsang
%date: 2021-08-24

# Why Configuration file?

Each nginx configuration file is responsible
for one website. It contains information
about where your website files are
(if your website is static) or which
port should it listen to (if your
website is dynamic). All configuration files
are located at the directory /etc/nginx/conf.d

-------------------------------------------------

-> # Config files generator <-

For saving time and safing the sanity of the
Fezent employees, you can use the fezent command
to generate the right config file for your website.

```
addconf example-site.conf -static
addconf # Enter this command to learn more
```

-------------------------------------------------

-> # Adding config files manually <-

-> ## Static site: <-
```
server {
  listen 80;
  server_name [DOMAIN_NAME]; 
  root /www/[FOLDER_NAME];
  index [INDEX_FILE];
  location / {
    try_files $uri $uri/ =404;
  }
}
```

-> ## Dynamic site: <-

```
server {
  listen 80;
  server_name [DOMAIN_NAME];
  client_max_body_size 32m;
  location / {
    proxy_pass http://127.0.0.1:[PORT];
  }
}
```

If your site use websocket
you need to add the followiong 2 lines in the location block:

```
proxy_set_header Upgrade $http_upgrade;
proxy_set_header Connection "Upgrade";
```

-------------------------------------------------

# The whole story

One day a fezent landed in front of a computer.
She typed fezent.com on Chrome.

Since the domain name fezent.com has configured in 
its DNS control panel(can be namecheap, digitalocean 
or scaleway etc.) to point to the address of 
the fezent server: 51.158.190.194,
now the fezent arrived where the fezent server is located at.

But the fezent server is a complex building,
it has other domain name pointing to it,
like tsangheiyiu.com, istigkeit.xyz and so forth.
The fezent is lost, not knowing which floor and
where in the building she should go.

Here Nginx came to rescue.
He sits at the lobby and tell the fezent that she
should go to /www/fezent.com where the content of
the fezent website is located. But how do Nginx 
know these things? Because the residencies has
given Nginx some cheatsheets as to where things
are and how should Nginx act accordingly.
These cheatsheet is the configuration files.
Located at the directory /etc/nginx/conf.d

-------------------------------------------------

# Another story

Another fezent arrived at the fezent server
wanting to meet Anthony, so he asked nginx
to go to tsangheiyiu.com. However tsangheiyiu.com
is a dynamic website, so the file serving is done
by Anthony running a program in his resident
to serve the content to a port near the shore.
This time, Nginx won't bring the fezent to where
the files are, instead he bring her to the right
port where the files are transported there.

Press *q* to exit this tutorial.
