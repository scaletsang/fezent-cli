
Fezent Kindergarden program
===========================
I am your bitch and your tutor. Here you can find documentations and guides about adding serving a website on this server, adding ssl certificate to your sites, and if you are still up, a little bit of bash commands. The world is dangerous only because you choose to let it be that way.

Usage: howtofezent [-TOPICS]
  -TOPICS:
    -certbot | -ssl | -https    How to add https to your site?
    -addconf                    How to add configuration file to the server so that your website can be published like a piece of cake?
    -static | --static-site     A guide to serve a static website.
    -dynamic | --dynamic-site   A guide to serve a dynamic website.
    -bash | -cli                Very easy-to-follow guide about how to use your command line.
    -h | --help                 Shows this menu.
