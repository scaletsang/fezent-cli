%title: How to secure your website connection (https)?
%author: Anthony Tsang
%date: 2021-08-24

-> # A guide to certbot <-

-> 1. Follow the guide on the [certbot website](https://certbot.eff.org/lets-encrypt/ubuntubionic-nginx) <-

-------------------------------------------------

2. Until the step where you need to get or
install a certificate. It will generate certificate
in the protected path/etc/letsencrypt/live and
also inject some config commands in the
corresponding conf files for each domain. 

## Generate 1 certificate for 1 domain 
(Possibly it will ask you which domain(s) 
would you like to generate a certificate for.):

```
sudo certbot --nginx
```

## Generate 1 certificate for multiple domains:

```
sudo certbot --nginx -d first-domain.com -d second-domain.hk
```

-------------------------------------------------

3. Step for using rack-ssl-enforcer that allow
sinatra or other rack frameworks to use ssl connection.
In the conf file of the domain, add the following lines:

```
server {
	location / {
		proxy_set_header X-Forwarded-Proto https;
		proxy_set_header Host $http_host;
		proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
		proxy_redirect off;
	}
}
```

Press *q* to exit this guide.
