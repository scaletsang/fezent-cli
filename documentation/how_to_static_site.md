%title: How to serve a static site?
%author: Anthony Tsang
%date: 2021-08-24

-> # Serve a static site <-

1. Put your project inside /www
2. Write a config file inside /etc/nginx/conf.d
   to specify where your project folder is and
   other relevant details.

See, simple!

Turn to next pages to see how to do that.

-------------------------------------------------

-> # 1. Put your project inside /www <-

For example you have a project folder called
dirtminer.com in your desktop folder at your
local machine.

_Using sftp_
```
# on your local machine
cd desktop
sftp user@fezent.com
# now you are at the server
cd /www
put -r dirtminer.com
```

_Using git_
```
ssh someone@fezent.com
cd /www
git clone https://gitlab.com/someone/dirtminer.git
```

-------------------------------------------------

-> # 2. Generate config file for nginx <-

Run the configuration file generator for static site
(the following command). It will ask you for
1. your domain name
2. project folder name that are located in /www
3. the entry index file.

```
addconf dirtminer.com -static
```

After you filled out the info, the generator will
generate the following conf file in /etc/nginx/conf.d
For example:

dirtminer.com.conf
```
server {
  listen 80;
  server_name dirtminer.com; 
  root /www/dirtminer.com;
  index index.html;
  location / {
    try_files $uri $uri/ =404;
  }
}
```

Press *q* to exit this tutorial.
