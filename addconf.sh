#!/bin/bash
source "$(dirname "${BASH_SOURCE[0]}")/config.sh"

addconf() {
  local conf_name=$1
  local type=$2
  local conf_path="${NGINX_CONF_PATH}/${conf_name}"
  if [ -f $conf_path ]; then
    return "file existed. Please choose another name or removed the target config file first."
  fi
  case $type in
    -static)
      echo "Next up you will be asked to enter your domain name, site folder name, and the index html name"
      __read-echo DOMAIN_NAME "1. Enter your domain name. e.g. tsangheiyiu.com"
      __read-echo FOLDER_NAME "2. Enter your site folder name stored in /www."
      __read-echo INDEX_FILE "3. Enter the entry html of your site. e.g. index.html"
      __write-conf-without-overwrite $conf_path "static.conf"
      unset DOMAIN_NAME FOLDER_NAME INDEX_FILE;;
    -dynamic)
      echo "Next up you will be asked to enter your domain name and the port"
      __read-echo DOMAIN_NAME "1. Enter your domain name. e.g. tsangheiyiu.com"
      __read-echo PORT "2. Enter the port that nginx should listen to. e.g. 9240"
      __write-conf-without-overwrite $conf_path "dynamic.conf"
      unset DOMAIN_NAME PORT;;
    -websocket)
      echo "Next up you will be asked to enter your domain name and the port"
      __read-echo DOMAIN_NAME "1. Enter your domain name. e.g. tsangheiyiu.com"
      __read-echo PORT "2. Enter the port that nginx should listen to. e.g. 9240"
      __write-conf-without-overwrite $conf_path "websocket.conf"
      unset DOMAIN_NAME PORT;;
    *|-h|--help)
      cat "${DOCUMENTATION_PATH}/config_file_generator.md";;
  esac
}

__read-echo() {
  local message=$2
  echo $message
  read $1
}

__read-from-template() {
  local path=$1
  local template="$(cat ${path})"
  eval "echo \"${template}\""
}

__write-conf() {
  local render_path=$1
  while read; do
    echo "$REPLY" >> $render_path
  done
}

__write-conf-without-overwrite() {
  local render_path=$1
  local template_name=$2
  __read-from-template "${TEMPLATE_PATH}/${template_name}" | __write-conf $conf_path
  echo "Configuration file generated at ${render_path}. You can also type 'fezent -conf' to teleport to the folder."
}