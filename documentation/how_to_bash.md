%title: A simple guide to shell commands
%author: Anthony Tsang
%date: 2021-08-24

-> # All useful commands <-

## Directory Navigation
```
 cd [path]
 ls
 pwd
```
## Read file
```
 cat [file]
 less [file]
```
## Create files and directory
```
 touch [file1] [file2]
 mkdir [directory]
```
## Write files
```
 vim [file]
 echo [text] > [file]
```
## Manipulate files and directory
```
 rm [file]
 mv [file] [directory]
 cp [file1] [file2] [directory]
```
## Search and multiple terminal screens
```
 grep [pattern] [file]
 screen
```
## Package installation
```
 git
 apt-get [package]
 snap install [package]
```

-------------------------------------------------

-> # Directory Navigation <-

-> Walk around the server, <-
-> Wanna *cd*? Change direction? <-
-> Can you write me a *ls* of things you see? <-
-> Wait... where am I heading? God can you tell me?<-
-> God can you *p*-rint me my *w* alking *d*-irection <-
-> Great, now I am not lost! <-

```
 cd        # back to your home directory
 cd ..     # go to the parent of the current directory
 cd path   # go to a specific directory

 ls        # list the files of the current directory
 ls path   # list the files of the specific directory
 ls -a     # list files including the hidden files
 ls -la    # list files and their files permissions

 pwd       # Print Working Directory
```

-------------------------------------------------

-> # Read files <-

-> See the files with *ls*? Oh, you want to see their contents? <-
-> Peek into it like a *cat*. <-
-> AHHHH the file is too long, I want to see *less*~ <-
-> Wow now I see segment of the file one by one <-

```
 cat filename
 less filename
```

 e.g.
```
 cat myproject/file.txt
 less anotherfile.md
```

-------------------------------------------------

-> # Create files and directory <-

-> With *cat*, *cd* and *ls* you can see <-
-> but nothing can be made without *touch* <-
-> Now you are blessed with the hands of god, <-
-> Why not make your own path? <-
-> First dirt, then a ground, finally a path <-
-> *mk* your *dir*-t! <-

```
 touch file
 touch file1 file2
 mkdir directory
 mkdir directory1 directory2
```

 e.g. Create a simple website structure
```
 touch index.html            # made a file in the root folder
 mkdir css js media          # made 3 folders
 cd js                       # go inside the folder called js
 touch index.js animation.js # made 2 files in the folder js
 cd ..                       # go back to the root folder
 touch css/stylesheet.css    # made a file inside the folder css
```

_Result:_
/root-folder
  - index.html
  - /media
  - /js
    - index.js
    - animation.js
  - /css
    - stylesheet.css

-------------------------------------------------

-> # The Shit: Write file <-

-> This is the meat on your journey to world creation. <-
-> *Vi* is the sign of victory. Vim is a text editor in your terminal. <-
-> Victory wouldn't be a victory if it is easy to achieved. <-
-> But you will get there. Let's go! <-

```
 sudo vim filename
```

 Landed inside the vim text editor, you will be in _normal mode_.
 In this mode, you cannot type,
 but you can edit the file using commands. For example:
```
 2dd  # Delete & copy 2 lines (3dd would be 3 lines, 10dd would be 10... etc.)
 2yy  # Copy 2 lines (3yy would be 3 lines, 10yy would be 10... etc.)
 p    # Paste copied line(s)
 :w   # Save any changes you did to the file
 :q   # Exit vim text editor
 :wq  # Save changes and exit
 i    # Enter Insert mode
```

 When you type *i*, you will enter the _insert mode_.
 Now you can finally type in the file!
 When you are done, press *Esc* can you will be back to _insert mode_.
 Type *:wq* and you will save and exit the text editor.

 For more tutorials, there is a built-in tutorial in the terminal:
```
 vimtutor
```

Write without vim!!:
You can also write to a file using *>*:
```
echo 'new line' >> file  # Append the text to the end of the file with a new line
echo 'new line' > file   # replace all contents of the file with the new text
```

-------------------------------------------------

-> # Move, Remove & Copy that? <-

-> Finally, *Vi*-ctory. <-
-> You are the champion. <-
-> But life continues. <-
-> You no longer fight, but lay in bed watching anime. <-
-> You get fatter. Your partner asked you to *mv* your ass. <-
-> "You are blocking the tv! *MV* YOUR ASSSSS! *CP* that?" <-
-> "I will I will...after I finish my popcorn" <-
-> "I SEE~~ SO YOU REALLY WANT ME TO *RM* <-
-> YOUR EXISTENCE FROM THIS PLANET?" <-

```
 mv filename/directory target-directory
 mv filename new-filename  # mv is also used to rename file or folder
 cp file1 file2 target-directory
 rm filename
 rm -r foldername
```

 e.g.
```
 mv your-ass.jpeg the/side             # a jpeg is moved to a folder
 cp your-gf-words.txt you/little-brain # a text file is copied to a folder
 rm your-dick.sh                       # your-dick.sh is removed
 mv your-wife your-divorced-wife       # A folder is renamed
 rm -r you                             # A folder named you is removed
```

-------------------------------------------------

-> # In search of a home <-

-> Luckily the world system called *git* (next page) saved your ass. <-
-> You are revived from the trash bin. <-
-> You are sad and pathetic. <-
-> *Path*-etic because you cannot find one. <-
-> "Get a *grep*! A lot of people could not even walk in the server!" <-
-> "Now stand up and search for your path!" <-

Grep allows you to search keyword from a file or directory.
```
 grep pattern file
 grep -r pattern directory
```

 e.g.
```
 grep meaning life.txt       # Search for the word 'meaning' in the file
 grep -r path universe/earth # Search for the word 'path' in the directory
```

-------------------------------------------------

-> # SCREAMMMMMMMM <-

-> The story continues <-
-> "Now stand up and search for your path!" <-
-> *screen*-ed your fezent with the loudest cluck he can make. <-
-> His *screen* is so strong that your consciousness split. <-
-> into two, and then three, and then so many. <-
-> You became a crew of people finding their path in life. <-

Screen split your terminal into multiple screens,
and you can switch between them when you feels like.
```
 screen                      # Enter a screen
 screen -ls                  # Show all screens with their id
 screen -r 32014             # Reattach to a screen with a id of the screen
```

Inside a screen, you can do whatever you can do in a normal terminal,
and also screen actions, including:

Keys to press  actions
*Ctrl* a c     Add a screen
*Ctrl* a n     Switch between all screens
*Ctrl* a d     Detach from all screens, return to original terminal

-------------------------------------------------

-> # Package Instalation <-

-> Remember me? I saved you from the trash bin. <-
-> Let me introduce myself. Konigiwa, I am *git*. <-
-> I can help you download files from open-source projects <-
-> that are located at a server with git installed. <-

-> Let me introduce myself. Bonjour, I am *apt-get*. <-
-> Let me introduce myself. 你好, I am *snap*. <-
-> We are the package store in linux!

Install packages from apt-get or snap
```
 sudo apt-get package-name
 sudo snap install package-name
```

Download a project from Github/Gitlab/server file using git.
In this example, git is downloading from Anthony's gitlab project.
```
 git clone https://gitlab.com/scaletsang/fezent-cli.git
```

Read changes from Github/Gitlab/server if it is changed
```
 git pull
```

To know how to use git in your website development environment,
checkout this [link](https://rogerdudler.github.io/git-guide/).

Press *q* to exit this guide.
