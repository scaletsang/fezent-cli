# The Fezent command line tool

## fezent

```bash
fezent -conf    # Go to the nginx config files dir
fezent -www     # Go to the static files dir
fezent -reload  # Reload Nginx
fezent -learn   # alias for howtofezent
fezent -cluck   # Fezent Cluck!
```

## addconf (Nginx config file generator)

```bash
addconf [site-name-conf-file-name] [-static|-dynamic|-websocket]
addconf www.example.com.conf -static # Example for static site config file
```

## howtofezent

```bash
howtofezent -certbot # A guide about certbot setup
howtofezent -addconf # A guide about nginx conf files
howtofezent -static  # A guide about serving static site
howtofezent -dynamic # A guide about serving dynamic site
howtofezent -bash    # A guide about using commands in terminal.
```

Dependency: [mdp, a terminal markdown presentation viewer](https://github.com/visit1985/mdp)