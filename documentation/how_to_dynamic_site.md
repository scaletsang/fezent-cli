%title: How to serve a dynamic site?
%author: Anthony Tsang
%date: 2021-08-24

-> # Serve a dynamic site <-

1. Run a server at your project folder, and
   serve it at a specific port.
2. Write a config file inside /etc/nginx/conf.d
   to specify which port should nginx go to and
   grab the project files upon client reaching
   the server address with the corresponding
   domain name.

The first step requires you to know how to code
a server backend.

However, the second step is documented next page.

-------------------------------------------------

-> # Generate config file for nginx <-

For example you have a project folder named
tsangheiyiu.com

Run the configuration file generator for static site
(the following command). It will ask you for
1. your domain name
2. port that your project files is serving to

```
addconf tsangheiyiu.com -dynamic
```

If your dynamic site need to use websocket, type instead:
```
addconf tsangheiyiu.com -websocket
```

After you filled out the info, the generator will
generate the following conf file in /etc/nginx/conf.d
For example:

tsangheiyiu.com.conf
```
server {
  listen 80;
  server_name tsangheiyiu.com;
  client_max_body_size 32m;
  location / {
    proxy_pass http://127.0.0.1:9000; # here 9000 is the port
  }
}
```

Press *q* to exit this tutorial.
