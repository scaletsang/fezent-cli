#!/bin/bash
source "$(dirname "${BASH_SOURCE[0]}")/config.sh"
source "$(dirname "${BASH_SOURCE[0]}")/addconf.sh"

fezent() {
  case $1 in
    -conf|--config-domains)
      cd $NGINX_CONF_PATH && ls;;
    -reload|--reload-nginx)
      sudo nginx -s reload
      echo "nginx reloaded";;
    -www|--static-files)
      cd $WWW_PATH && ls;;
    -learn)
      howtofezent $2;;
    -cluck)
      __read-from-template "${DOCUMENTATION_PATH}/cluck.md";;
    *|-h|--help)
      cat "${DOCUMENTATION_PATH}/fezent.md";;
  esac
}

howtofezent() {
  case $1 in
    -certbot|-ssl|-https)
      mdp -t "${DOCUMENTATION_PATH}/how_to_certbot.md";;
    -addconf)
      mdp -t "${DOCUMENTATION_PATH}/how_to_add_conf.md";;
    -static|--static-site)
      mdp -t "${DOCUMENTATION_PATH}/how_to_static_site.md";;
    -dynamic|--dynamic-site)
      mdp -t "${DOCUMENTATION_PATH}/how_to_dynamic_site.md";;
    -bash|-cli)
      mdp -t -f "${DOCUMENTATION_PATH}/how_to_bash.md";;
    *|-h|--help)
      cat "${DOCUMENTATION_PATH}/how_to_fezent.md";;
  esac
}