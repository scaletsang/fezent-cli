
Fezent site config file generator
=================================

Depending on whether you want a static website, dynamic website, or a dynamic website with websocket connection, the generator will generate the corresponding template.

Usage: addconf [config-file-name] [-TEMPLATE-TYPE]
e.g. addconf tsangheiyiu.com.conf -dynamic

-TEMPLATE-TYPE: 
  -static
  -dynamic
  -websocket

"fezent -addconf" is deprecated, please use "addconf"
